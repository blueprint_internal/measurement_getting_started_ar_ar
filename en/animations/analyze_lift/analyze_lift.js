
function Analyze_Lift(resources)
{
	Analyze_Lift.resources = resources;
}
Analyze_Lift.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(900, 400, Phaser.CANVAS, 'Analyze_Lift', { preload: this.preload, create: this.create, update: this.update, render:
		this.render,parent:this });
	},

	preload: function()
	{

		this.game.scale.maxWidth = 900;
    	this.game.scale.maxHeight = 400;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('placement', Analyze_Lift.resources.placement);
		this.game.load.image('people_green', Analyze_Lift.resources.people_green);
		this.game.load.image('people_blue', Analyze_Lift.resources.people_blue);
		this.game.load.image('arrow', Analyze_Lift.resources.arrow);
		this.game.load.image('lift', Analyze_Lift.resources.lift);
		this.game.load.image('computer', Analyze_Lift.resources.computer);
		this.game.created = false;
    	this.game.stage.backgroundColor = '#ffffff';
    	this.game.antialias = false;

	},

	create: function(evt)
	{

		if(this.game.created === false)
		{
			//this.parent.placement = this.game.add.sprite(0,0,'placement');

            this.parent.game.stage.backgroundColor = '#ffffff';
			this.line = this.game.add.graphics(12,60);


		    this.line.lineStyle(1, 0x000000);
		    this.line.moveTo(0,0);
		    this.line.lineTo(845,0);
		    this.line.endFill();

		    this.parent.treatment_people = this.game.make.sprite(0,0,'people_blue');
		    this.parent.treatment_people.anchor.set(0.5,0.5);
		    this.parent.control_people = this.game.make.sprite(0,0,'people_blue');
		    this.parent.control_people.anchor.set(0.5,0.5);
		    this.parent.green_people_1 = this.game.make.sprite(0,0,'people_green');
		    this.parent.green_people_1 = this.game.make.sprite(0,0,'people_green');
		    this.parent.computer = this.game.make.sprite(0,0,'computer');
		    this.parent.computer.anchor.set(0.5,0.5);
		    this.parent.lift = this.game.add.sprite(0,0,'lift');
		    this.parent.lift.anchor.set(0.5,0.5);

	    	this.game.created  = true;
	    	this.parent.buildAnimation();
	    }
	},

	buildAnimation: function()
	{
		var style = Analyze_Lift.resources.style_1;
		var style2 = Analyze_Lift.resources.style_2;
		this.headers = [];
		for(var i = 0;i<4;i++)
		{
			var temp = this.game.add.text(170+(190*i),35, Analyze_Lift.resources["title_"+(i+1)],style);
			temp.smoothed = false;
			temp.anchor.set(0.5,0.5);
			this.headers.push(temp);
		}
		//headers[1].x-=25;
		//headers[2].x-=50;
		//headers[3].x+=50;
	    this.treatment_group = this.game.add.group();
	    this.treatment_group.alpha = 0;
	    this.treatment_text= this.game.make.text(0,0, Analyze_Lift.resources.subject_1,style2);
	    this.treatment_text.anchor.set(0.5,0.5);
	    this.treatment_group.add(this.treatment_people);
	    this.treatment_text.y=this.treatment_people.height-30;
	    this.treatment_group.add(this.treatment_text);
	    this.treatment_group.x = 160;
	    this.treatment_group.y = 125;
	    this.treatment_text.smoothed = false;



	    this.control_group = this.game.add.group();
	    this.control_text= this.game.make.text(0,0, Analyze_Lift.resources.subject_2,style2);
	    this.control_text.anchor.set(0.5,0.5);
	    this.control_group.add(this.control_people);
	    this.control_text.y=this.control_people.height-30;
	    this.control_group.add(this.control_text);
	    this.control_group.x = 160;
	    this.control_group.y = 290;
	    this.control_text.smoothed = false;

	    this.media_1_group = this.game.add.group();
	    this.media_1_text= this.game.make.text(0,0, Analyze_Lift.resources.subject_3,style2);
	    this.media_1_text.anchor.set(0.5,0.5);
	    this.media_1_group.add(this.computer);
	    this.media_1_text.y=this.computer.height;
	    this.media_1_text.smoothed = false;
	    this.media_1_group.add(this.media_1_text);
	    this.media_1_group.x=370;
	    this.media_1_group.y=110;

		this.media_2_text= this.game.add.text(0,0, Analyze_Lift.resources.subject_4,style2);
		this.media_2_text.anchor.set(0.5,0.5);
		this.media_2_text.smoothed = false;
		this.media_2_text.x=370;
		this.media_2_text.y=300;



	    this.media_2_group = this.game.add.group();




	    this.arrow_1 = this.game.add.sprite(0,0,'arrow');
	    this.arrow_2 = this.game.add.sprite(0,0,'arrow');
	    this.arrow_3 = this.game.add.sprite(0,0,'arrow');
	    this.arrow_1.anchor.set(0.5,0.5);
	    this.arrow_2.anchor.set(0.5,0.5);
	    this.arrow_3.anchor.set(0.5,0.5);

	    this.arrow_1.y = 205;
	    this.arrow_2.y = 205;
	    this.arrow_3.y = 205;

	    this.arrow_1.x = 280;
	    this.arrow_2.x = 465;
	    this.arrow_3.x = 680;
	    this.deliver_1_group = this.game.add.group();
	    this.deliver_2_group = this.game.add.group();

	    this.green_1 = this.game.add.sprite(0,0,'people_green');
	    this.green_1.anchor.set(0.5,0.5);
	    this.green_2 = this.game.add.sprite(0,0,'people_green');
	    this.green_2.anchor.set(0.5,0.5);
	    this.green_1.x = 570;
	    this.green_1.y = 125;
	    this.green_2.x = 570;
	    this.green_2.y = 290;

	    this.lift.x = 790;
	    this.lift.y = 210;

	    this.treatment_group.x=-120;
		this.treatment_group_anim = this.game.add.tween(this.treatment_group).to({alpha:1,x: 160},900,Phaser.Easing.Quadratic.Out);

		this.control_group.x=-120;
		this.control_group_anim = this.game.add.tween(this.control_group).to({alpha:1,x: 160},900,Phaser.Easing.Quadratic.Out);

		this.arrow_1.x-=30;
		this.arrow_1.alpha = 0
		this.arrow_1_anim = this.game.add.tween(this.arrow_1).to({alpha:1,x: 280},800,Phaser.Easing.Quadratic.Out);


		this.media_1_group.y-=40
		this.media_1_group.alpha = 0;
		this.media_1_group_anim = this.game.add.tween(this.media_1_group).to({alpha:1,y: 110},800,Phaser.Easing.Quadratic.Out);

		this.media_2_text.y+=40
		this.media_2_text.alpha = 0;
		this.media_2_anim = this.game.add.tween(this.media_2_text).to({alpha:1,y: 300},800,Phaser.Easing.Quadratic.Out);

		this.green_1.x+=100
		this.green_1.alpha = 0;
		this.green_1_anim = this.game.add.tween(this.green_1).to({alpha:1,x: 570},900,Phaser.Easing.Quadratic.Out);

		this.arrow_2.x-=30;
		this.arrow_2.alpha = 0
		this.arrow_2_anim = this.game.add.tween(this.arrow_2).to({alpha:1,x: 465},800,Phaser.Easing.Quadratic.Out);

		this.green_2.x+=100
		this.green_2.alpha = 0;
		this.green_2_anim = this.game.add.tween(this.green_2).to({alpha:1,x: 570},900,Phaser.Easing.Quadratic.Out);

		this.arrow_3.x-=30;
		this.arrow_3.alpha = 0
		this.arrow_3_anim = this.game.add.tween(this.arrow_3).to({alpha:1,x: 680},800,Phaser.Easing.Quadratic.Out);

		this.lift.y-=30;
		this.lift.alpha = 0;
		this.lift_anim = this.game.add.tween(this.lift).to({alpha:1,y:210},800,Phaser.Easing.Quadratic.Out);

		//headers
        var hldY = this.headers[0].y
		this.headers[0].y-=30;
		this.headers[0].alpha = 0;
		this.headers_1_anim = this.game.add.tween(this.headers[0]).to({alpha:1,y:hldY},500,Phaser.Easing.Quadratic.Out);

		var hldY2 = this.headers[1].y
		this.headers[1].y-=30;
		this.headers[1].alpha = 0;
		this.headers_2_anim = this.game.add.tween(this.headers[1]).to({alpha:1,y:hldY2},500,Phaser.Easing.Quadratic.Out);

		var hldY3 = this.headers[2].y
		this.headers[2].y-=30;
		this.headers[2].alpha = 0;
		this.headers_3_anim = this.game.add.tween(this.headers[2]).to({alpha:1,y:hldY3},500,Phaser.Easing.Quadratic.Out);

		var hldY4 = this.headers[3].y
		this.headers[3].y-=30;
		this.headers[3].alpha = 0;
		this.headers_4_anim = this.game.add.tween(this.headers[3]).to({alpha:1,y:hldY4},500,Phaser.Easing.Quadratic.Out);



		this.headers_1_anim.chain(this.treatment_group_anim,this.control_group_anim,this.arrow_1_anim,this.headers_2_anim,this.media_1_group_anim,this.media_2_anim,this.arrow_2_anim,this.headers_3_anim,this.green_1_anim,this.green_2_anim,this.headers_4_anim,this.arrow_3_anim,this.lift_anim);


		this.headers_1_anim.start();

		/*

		//this.arrow_1_anim.start();
		this.leftSphereGroup.x = -250;
		this.rightSphereGroup.x = 1150;
		this.arrowGroup_2.y = 500;
		this.leftSphere_anim = this.game.add.tween(this.leftSphereGroup).to({alpha:1,x: this.leftSphereGroup.origins.x},1000,Phaser.Easing.Quadratic.Out);
		this.rightSphere_anim = this.game.add.tween(this.rightSphereGroup).to({alpha:1,x: this.rightSphereGroup.origins.x},1000,Phaser.Easing.Quadratic.Out);
		this.rightGroup.y = this.rightGroup.origins.y-50;
		this.leftGroup.y = this.rightGroup.origins.y-50;
		this.leftGroupAnim = this.game.add.tween(this.leftGroup).to({alpha:1,y:this.leftGroup.origins.y},1000,Phaser.Easing.Quadratic.Out);
		this.rightGroupAnim = this.game.add.tween(this.rightGroup).to({alpha:1,y:this.rightGroup.origins.y},1000,Phaser.Easing.Quadratic.Out);
		this.arrow_2_anim = this.game.add.tween(this.arrowGroup_2).to({alpha:1,y: this.arrowGroup_2.origins.y},900,Phaser.Easing.Quadratic.Out);

		this.leftSphere_anim.chain(this.rightSphere_anim,this.leftGroupAnim,this.rightGroupAnim,this.arrow_2_anim,this.arrow_1_anim);
		this.leftSphere_anim.start();
		*/

	},
	inview: function()
	{


	},

	animate: function()
	{
		//console.log("animate")
	},

	update: function()
	{

	},
	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}
